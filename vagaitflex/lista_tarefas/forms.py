#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms
from lista_tarefas.models import tarefas

class FormTarefa(forms.ModelForm):
	titulo = forms.CharField(max_length=128, help_text="Título da tarefa")
	descricao = forms.CharField(max_length=500, help_text="Descricao")
	status = forms.CharField(max_length=1, help_text="Status")
	class Meta:
		model = tarefas
		fields = ('titulo','descricao','status')
