# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='tarefas',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(unique=True, max_length=128)),
                ('descricao', models.CharField(unique=True, max_length=500)),
                ('data_inicio', models.DateTimeField(verbose_name=b'Data de in\xc3\xadcio da tarefa')),
                ('data_encerramento', models.DateTimeField(verbose_name=b'Data de encerramento da tarefa')),
                ('status', models.IntegerField(default=0)),
            ],
        ),
    ]
