# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lista_tarefas', '0002_perfil_usuario'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='perfil_usuario',
            name='user',
        ),
        migrations.DeleteModel(
            name='perfil_usuario',
        ),
    ]
