# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lista_tarefas', '0003_auto_20150930_2322'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tarefas',
            name='data_encerramento',
        ),
        migrations.RemoveField(
            model_name='tarefas',
            name='data_inicio',
        ),
    ]
