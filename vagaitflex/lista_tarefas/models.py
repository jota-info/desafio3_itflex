#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User

class tarefas(models.Model):
	titulo = models.CharField(max_length=128, unique=True)
	descricao = models.CharField(max_length=500, unique=True)
	status = models.IntegerField(default=0)