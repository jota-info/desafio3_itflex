#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from models import tarefas 

class TarefasSerializer(serializers.ModelSerializer):
    class Meta:
        model = tarefas