#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.conf.urls import patterns, url
# from django.contrib import admin
# from lista_tarefas.views import *

# urlpatterns = patterns('',
# 	url(r'^$', Index.as_view(), name='index'),
# 	url(r'^rest/tarefas/', GetTarefasRestView.as_view(), name='rest_index'),
# 	url(r'^rest/detalhes/(?P<tarefa_id>\d+)/$', GetDetalhesRestView.as_view(), name='rest_detalhes'),
# 	url(r'^sobre/', Sobre.as_view(), name='sobre'),
# 	url(r'^detalhes/(?P<tarefas_id>\d+)/$', Detalhes.as_view(), name='detalhes'),
# 	url(r'^adicionar_tarefa/', Adicionar_tarefa.as_view(), name='adicionar_tarefa'),
# 	url(r'^rest/adicionar_tarefa/', AdicionarTarefaRestView.as_view(), name='rest_adicionar_tarefa'),
# 	url(r'^alterar_tarefa/(?P<tarefas_id>\d+)/$', Alterar_tarefa.as_view(), name='alterar_tarefa'),
# 	url(r'^excluir_tarefa/(?P<tarefas_id>\d+)/$', Excluir_tarefa.as_view(), name='excluir_tarefa'),
# 	url(r'^rest/excluir_tarefa/(?P<tarefas_id>\d+)/$', DeleteTarefaRestView.as_view(), name='rest_excluir_tarefa'),
# 	)

