#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from lista_tarefas.models import tarefas
from lista_tarefas.forms import CriarTarefa
from django.views.generic.base import View
from serializer import TarefasSerializer
from rest import JSONResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
from django.contrib.auth.models import User

class Index(View):
	template = 'lista_tarefas/index.html'

	def get(self, request):
		dicionario_conteudo = {'boasvindas': 'Aproveite bem nosso sistema'}
		tarefas_cadastradas = tarefas.objects.all()
		dicionario_conteudo = {'tarefas': tarefas_cadastradas}
		return render(request, self.template, dicionario_conteudo)

class Sobre(View):
	template = 'lista_tarefas/sobre.html'
	
	def get(self, request):
		return render(request, self.template)

class GetTarefasRestView(APIView):
	permission_classes = (AllowAny,)

	def get(self, request):
		tarefas_cadastradas = tarefas.objects.all()
		lista_tarefas_cadastradas = []
		
		for tarefa in tarefas_cadastradas:
			serializer = TarefasSerializer(tarefa)
			lista_tarefas_cadastradas.append(serializer.data)
		return JSONResponse(lista_tarefas_cadastradas)

class Detalhes(View):
	template = 'lista_tarefas/detalhes.html'
	
	def get(self, request, tarefas_id):
		dicionario_conteudo = {}
		try:
			tarefa_detalhes = tarefas.objects.get(id=tarefas_id)  #onde = id
			dicionario_conteudo['Tarefa'] = tarefa_detalhes
		except:
			pass
		return render(request, self.template , dicionario_conteudo)

class GetDetalhesRestView(APIView):
	permission_classes = (AllowAny,)

	def get(self, request, tarefa_id):
		tarefa = tarefas.objects.get(id=tarefa_id)
		detalhes_tarefa = []
		serializer = TarefasSerializer(tarefa)
		detalhes_tarefa.append(serializer.data)
		return JSONResponse(detalhes_tarefa)

class Adicionar_tarefa(View):
    template = 'lista_tarefas/adicionar_tarefa.html'
    
    def post(self, request):
        form = CriarTarefa(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return redirect("/lista_tarefas/")
        else:
            print form.errors
            return render(request, self.template, {'form': form})

    def get(self, request):
     	form = CriarTarefa()
    	return render(request, self.template, {'form': form})

class AdicionarTarefaRestView(APIView):
	permission_classes = (AllowAny,)
	
	def post(self, request):
		tarefa_serializer = TarefasSerializer(request.data)
		tr = tarefas()
		tr.titulo = tarefa_serializer.data["titulo"]
		tr.descricao = tarefa_serializer.data["descricao"]
		tr.status = tarefa_serializer.data["status"]
		tr.save()
	
		return JSONResponse("Salvo com sucesso")


class Alterar_tarefa(View):
	template = 'lista_tarefas/alterar_tarefa.html'

	def post(self, request, tarefas_id):
		tarefa = tarefas.objects.get(pk = tarefas_id)
		formulario = CriarTarefa(instance=tarefa, data = request.POST)
		if formulario.is_valid():
			formulario.save()
			return redirect("/lista_tarefas/")
		else: 
			return render(request, self.template, {'form':formulario})

	def get(self, request, tarefas_id):
		tarefa = tarefas.objects.get(pk = tarefas_id)
		formulario = CriarTarefa(instance = tarefa)
		return render(request, self.template, {'form':formulario, 'tarefa_id':tarefa.id})


class Excluir_tarefa(View):
	template = 'lista_tarefas/Excluir_tarefa.html'

	def get(self, request, tarefas_id):
		tarefa = tarefas.objects.get(id = tarefas_id)
		tarefa.delete()
		redirect("/lista_tarefas/")
		return render(request, self.template)