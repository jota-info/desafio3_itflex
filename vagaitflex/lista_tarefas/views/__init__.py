# coding: utf-8

from lista_tarefas.views.views import Index 
from lista_tarefas.views.excluir_tarefa import Excluir_tarefa
from lista_tarefas.views.sobre import Sobre
from lista_tarefas.views.alterar_tarefa import Alterar_tarefa
from lista_tarefas.views.adicionarTarefaRest import AdicionarTarefaRestView
from lista_tarefas.views.adicionar_tarefa import Adicionar_tarefa
from lista_tarefas.views.getTarefasRest import GetTarefasRestView
from lista_tarefas.views.detalhes import Detalhes
from lista_tarefas.views.getDetalhesRest import GetDetalhesRestView
from lista_tarefas.views.excluirTarefaRest import DeleteTarefaRestView
from lista_tarefas.views.alterarStatusRest import AlterarStatusRestView