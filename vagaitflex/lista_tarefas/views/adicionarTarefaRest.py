#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from lista_tarefas.models import tarefas
from serializer import TarefasSerializer
from rest import JSONResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
import logging


class AdicionarTarefaRestView(APIView):
	permission_classes = (AllowAny,)
	logger = logging.getLogger(__name__)

	def post(self, request):
		tarefa_serializer = TarefasSerializer(request.data)
		tarefa_modelo_banco = tarefas()
		tarefa_modelo_banco.titulo = tarefa_serializer.data["titulo"]
		tarefa_modelo_banco.descricao = tarefa_serializer.data["descricao"]
		tarefa_modelo_banco.status = tarefa_serializer.data["status"]
		tarefa_modelo_banco.save()

		self.logger.debug("Adicionar tarefa via rest")
		return JSONResponse("Salvo com sucesso")
