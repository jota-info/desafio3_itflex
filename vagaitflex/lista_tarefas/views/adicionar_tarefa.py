#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from lista_tarefas.models import tarefas
from lista_tarefas.forms import FormTarefa
from django.views.generic.base import View
import logging


class Adicionar_tarefa(View):
    template = 'lista_tarefas/adicionar_tarefa.html'
    logger = logging.getLogger(__name__)

    def post(self, request):
        form = FormTarefa(request.POST)
        if form.is_valid():
            form.save(commit=True)
            self.logger.debug("Criação de nova tarefa: Formulário salvo")
            return redirect("/lista_tarefas/")
        else:
            print form.errors
            self.logger.errors("Falha ao criar nova tarefa");
            return render(request, self.template, {'form': form})

    def get(self, request):
     	form = FormTarefa()
        self.logger.debug("Get de formulário de criação")
    	return render(request, self.template, {'form': form})
