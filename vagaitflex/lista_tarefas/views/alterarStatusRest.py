#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from lista_tarefas.models import tarefas
from serializer import TarefaStatus
from rest import JSONResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
import logging

class AlterarStatusRestView(APIView):
	logger = logging.getLogger(__name__)
	permission_classes = (AllowAny,)

	def post(self, request, tarefas_id):
		tarefa_modelo_banco = tarefas.objects.get(id=tarefas_id)
		tarefa_serializer = TarefaStatus(request.data)
		tarefa_modelo_banco.status = tarefa_serializer.data["status"]
		tarefa_modelo_banco.save()
		self.logger.debug("Adicionar tarefa via rest")
		return JSONResponse("Salvo com sucesso")