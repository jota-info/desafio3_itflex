#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from lista_tarefas.models import tarefas
from lista_tarefas.forms import FormTarefa
from django.views.generic.base import View
import logging

class Alterar_tarefa(View):
	logger = logging.getLogger(__name__)
	template = 'lista_tarefas/alterar_tarefa.html'

	def post(self, request, tarefas_id):
		tarefa = tarefas.objects.get(pk = tarefas_id)
		formulario = FormTarefa(instance=tarefa, data = request.POST)
		if formulario.is_valid():
			formulario.save()
			self.logger.debug("Alterar tarefa")
			return redirect("/lista_tarefas/")
		else:
			self.logger.error("Falha ao alterar tarefa") 
			return render(request, self.template, {'form':formulario})

	def get(self, request, tarefas_id):
		tarefa = tarefas.objects.get(pk = tarefas_id)
		formulario = FormTarefa(instance = tarefa)
		self.logger.debug("Formulário de tarefa específica")
		return render(request, self.template, {'form':formulario, 'tarefa_id':tarefa.id})
