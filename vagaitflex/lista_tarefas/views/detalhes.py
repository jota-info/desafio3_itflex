#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from lista_tarefas.models import tarefas
from django.views.generic.base import View

import logging


class Detalhes(View):
	logger = logging.getLogger(__name__)
	template = 'lista_tarefas/detalhes.html'
	
	def get(self, request, tarefas_id):
		dicionario_conteudo = {}
		try:
			tarefa_detalhes = tarefas.objects.get(id=tarefas_id)  #onde = id
			dicionario_conteudo['Tarefa'] = tarefa_detalhes
			
			self.logger.debug("Detalhes de uma tarefa em específica")
		except:
			pass
			self.logger.error("Falha ao requisitar uma tarefa em específico")
		return render(request, self.template , dicionario_conteudo)
