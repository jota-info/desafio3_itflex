#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from lista_tarefas.models import tarefas
from serializer import TarefasSerializer
from rest import JSONResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
import logging


class DeleteTarefaRestView(APIView):
	logger = logging.getLogger(__name__)
	permission_classes = (AllowAny,)

	def delete(self, request, tarefas_id):
		tarefa = tarefas.objects.get(id = tarefas_id)
		tarefa.delete()
		self.logger.debug("Exclusão de tarefa")
		return JSONResponse("Excluído")