#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from lista_tarefas.models import tarefas
from django.views.generic.base import View
import logging


class Excluir_tarefa(View):
	logger = logging.getLogger(__name__)
	template = 'lista_tarefas/Excluir_tarefa.html'

	def get(self, request, tarefas_id):
		tarefa = tarefas.objects.get(id = tarefas_id)
		tarefa.delete()
		self.logger.debug("Exclusão de tarefa")
		return redirect("/lista_tarefas/")