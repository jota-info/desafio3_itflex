#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from lista_tarefas.models import tarefas
from serializer import TarefasSerializer
from rest import JSONResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
import logging



class GetDetalhesRestView(APIView):
	logger = logging.getLogger(__name__)
	permission_classes = (AllowAny,)

	def get(self, request, tarefa_id):
		tarefa = tarefas.objects.get(id=tarefa_id)
		detalhes_tarefa = []
		serializer = TarefasSerializer(tarefa)
		detalhes_tarefa.append(serializer.data)
		self.logger.debug("Apresentação de uma tarefa específica completa")
		return JSONResponse(detalhes_tarefa)
