#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from lista_tarefas.models import tarefas
from serializer import TarefasSerializer
from rest import JSONResponse
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView
import logging


class GetTarefasRestView(APIView):
	logger = logging.getLogger(__name__)
	permission_classes = (AllowAny,)

	def get(self, request):
		tarefas_cadastradas = tarefas.objects.all()
		lista_tarefas_cadastradas = []
		
		for tarefa in tarefas_cadastradas:
			serializer = TarefasSerializer(tarefa)
			lista_tarefas_cadastradas.append(serializer.data)
			self.logger.debug("Lista de todas as tarefas")
		return JSONResponse(lista_tarefas_cadastradas)
