from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
import logging


class JSONResponse(HttpResponse):
	logger = logging.getLogger(__name__)
	def __init__(self, data, **kwargs):
		content = JSONRenderer().render(data)
		kwargs['content_type'] = 'application/json'
		super(JSONResponse, self).__init__(content, **kwargs)
		self.logger.debug("Json Response log")