#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from lista_tarefas.models.models import tarefas 

import logging


class TarefasSerializer(serializers.ModelSerializer):
	class Meta:
		logger = logging.getLogger(__name__)
		model = tarefas
		logger.debug("Serializador dos dados do banco para formato rest")

class TarefaStatus(serializers.ModelSerializer):
	class Meta:
		model = tarefas
		fields=('status',)
        		