#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.views.generic.base import View
from django.shortcuts import render

class Sobre(View):
	template = 'lista_tarefas/sobre.html'
	
	def get(self, request):
		return render(request, self.template)