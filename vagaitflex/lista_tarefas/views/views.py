#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from lista_tarefas.models import tarefas
from django.views.generic.base import View
import logging

class Index(View):
	logger = logging.getLogger(__name__)

	template = 'lista_tarefas/index.html'

	def get(self, request):
		dicionario_conteudo = {'boasvindas': 'Aproveite bem nosso sistema'}
		tarefas_cadastradas = tarefas.objects.all()
		dicionario_conteudo = {'tarefas': tarefas_cadastradas}
		self.logger.info("Informações do sistema ao abrir a página inicial")
		return render(request, self.template, dicionario_conteudo)





