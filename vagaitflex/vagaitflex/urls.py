from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings
#from lista_tarefas.views import *
from lista_tarefas.views import *

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^lista_tarefas/', include('lista_tarefas.urls')),
	url(r'^lista_tarefas/$', Index.as_view(), name='index'),
	url(r'^lista_tarefas/rest/tarefas/', GetTarefasRestView.as_view(), name='rest_index'),
	url(r'^lista_tarefas/rest/detalhes/(?P<tarefa_id>\d+)/$', GetDetalhesRestView.as_view(), name='rest_detalhes'),
	url(r'^lista_tarefas/sobre/', Sobre.as_view(), name='sobre'),
	url(r'^lista_tarefas/detalhes/(?P<tarefas_id>\d+)/$', Detalhes.as_view(), name='detalhes'),
	url(r'^lista_tarefas/adicionar_tarefa/', Adicionar_tarefa.as_view(), name='adicionar_tarefa'),
	url(r'^lista_tarefas/rest/adicionar_tarefa/', AdicionarTarefaRestView.as_view(), name='rest_adicionar_tarefa'),
	url(r'^lista_tarefas/alterar_tarefa/(?P<tarefas_id>\d+)/$', Alterar_tarefa.as_view(), name='alterar_tarefa'),
	url(r'^lista_tarefas/rest/alterar_status/(?P<tarefas_id>\d+)/$', AlterarStatusRestView.as_view(), name='rest_alterar_status'),
	url(r'^lista_tarefas/excluir_tarefa/(?P<tarefas_id>\d+)/$', Excluir_tarefa.as_view(), name='excluir_tarefa'),
	url(r'^lista_tarefas/rest/excluir_tarefa/(?P<tarefas_id>\d+)/$', DeleteTarefaRestView.as_view(), name='rest_excluir_tarefa'),

]

if settings.DEBUG:
    urlpatterns += patterns(
        'django.views.static',
        (r'^media/(?P<path>.*)',
        'serve',
        {'document_root': settings.MEDIA_ROOT}), )
